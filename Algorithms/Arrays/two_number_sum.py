def two_numbers_sum(array, target):
    """
    Return list of 2 numbers from array that sum ut to target integer
    or empty list otherwise if there are no two number in array which sum up to integer
    :param array: list of integers
    :param target: integer
    :return: list of 2 number or empty list
    """
    pairs = []
    complements = set()

    for number in array:
        complement = target - number
        if complement in complements:
            pairs.append(complement)
            pairs.append(number)
            return pairs
        else:
            complements.add(number)
    return pairs
