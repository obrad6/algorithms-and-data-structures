import unittest
from Algorithms.Arrays import two_number_sum as ts


class TestTwoNumberSum(unittest.TestCase):
    def test_two_number_sum_empty_list(self):
        self.assertListEqual(ts.two_numbers_sum([], 0), [])

    def test_two_number_sum_single_number(self):
        self.assertListEqual(ts.two_numbers_sum([2], 4), [])

    def test_two_number_sum_single_number_2(self):
        self.assertListEqual(ts.two_numbers_sum([3], 3), [])

    def test_two_number_sum_add_to_zero(self):
        self.assertListEqual(ts.two_numbers_sum([-2, 2], 0), [-2, 2])

    def test_two_number_sum_same_values(self):
        self.assertListEqual(ts.two_numbers_sum([4, 4], 8), [4, 4])

    def test_two_number_sum_1(self):
        self.assertListEqual(ts.two_numbers_sum([1, 2, 3, 4, 5], 5), [2, 3])

    def test_two_number_sum_2(self):
        self.assertListEqual(ts.two_numbers_sum([1, 2, 3, 4, 5], 3), [1, 2])

    def test_two_number_sum_2(self):
        self.assertListEqual(ts.two_numbers_sum([1, 2, 3, 4, 5], 9), [4, 5])