# Array Algorithms

1. ### Two Number Sum
   Write a function that takes in a non empty array of distinct integers and an integer representing a rarget sum. If any two integers in the input array sum up to the target sum, the function should return them in an array in any order. If no two numbers sum up to the target sum, the function should return an empty array. Target sum can't be obtained by summing up the number to itself and there will be at most one pair of numbers summing up to the target sum.
